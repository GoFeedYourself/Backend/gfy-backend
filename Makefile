.PHONY: migrate

migrate:
	@#@ Run migrations
	docker-compose run --rm migrator update
