FROM webdevops/liquibase:postgres

USER root

WORKDIR /liquibase

COPY ./docker/entrypoints/migrator.sh .
RUN chmod +x ./migrator.sh

WORKDIR changelog

COPY ./backend/app/models/changelog .

ENTRYPOINT ["../migrator.sh"]

CMD ["update"]
