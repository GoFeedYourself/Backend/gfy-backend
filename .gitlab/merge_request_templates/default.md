Reviewers: @reviewer

Optional Reviewers: @reviewer

 <!-- place related mr links here -->
Dependencies: no dependencies

## What it does:
- Adds some changes

## Regression area:
- List all the areas of the application that might be affected by this change

### Check yourself please:
- [ ] I've added all applicable labels to the MR
- [ ] I've checked MR regression area and it works as expected after my submission
- [ ] Merge Request changes fit definition of done

> By submitting this merge request, I confirm that all code works fine and I've added all the necessary tests to check the new functionality
