from abc import ABCMeta, abstractmethod
from typing import List

from domain.entities import FoodEntity


class IFoodRepository(metaclass=ABCMeta):

    @abstractmethod
    def get_list(self, limit: int = 100, skip: int = 0) -> List[FoodEntity]:
        pass
