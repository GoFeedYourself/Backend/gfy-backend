from dataclasses import dataclass, field
from typing import List


@dataclass
class FoodEntity:
    id: int
    name: str
    description: str
    price: float
    weight: str
    tags: List[str] = field(default_factory=list)
