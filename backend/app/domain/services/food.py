from typing import List

from domain.entities import FoodEntity
from domain.interfaces.repositories import IFoodRepository


class FoodService:
    def __init__(self, food_repository: IFoodRepository):
        self._food_repository = food_repository

    def get_list(self, limit: int = 100, skip: int = 100) -> List[FoodEntity]:
        return self._food_repository.get_list(limit=limit, skip=skip)
