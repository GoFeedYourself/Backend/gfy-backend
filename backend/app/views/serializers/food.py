from typing import List, Optional

from pydantic import BaseModel


class BaseFoodSchema(BaseModel):
    name: str
    description: str
    price: float
    weight: str
    tags: List[str]


class UpdateFoodSchema(BaseFoodSchema):
    name: Optional[str] = None
    description: Optional[str] = None
    price: Optional[float] = None
    weight: Optional[str] = None


class FoodSchema(BaseFoodSchema):
    id: int
