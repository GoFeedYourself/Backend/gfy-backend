from dataclasses import asdict
from typing import List

from fastapi import APIRouter

import containers
from views.serializers.food import FoodSchema

food_router = APIRouter()

food_service = containers.DomainServices.food()


@food_router.get('/', response_model=List[FoodSchema])
def get_food_list(limit: int = 100, skip: int = 0) -> List[FoodSchema]:
    food_list = food_service.get_list(limit=limit, skip=skip)
    return [
        FoodSchema(**asdict(food_entity)) for food_entity in food_list
    ]
