from fastapi import APIRouter
from .food import food_router

api_router = APIRouter()

api_router.include_router(food_router, prefix='/food', tags=['Food'])
