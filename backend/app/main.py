from fastapi import FastAPI
from config import settings
from database import configure_engine
from views import api_router

app = FastAPI(
    title=settings.PROJECT_NAME, openapi_url=f"{settings.API_STR}/openapi.json"
)

app.include_router(api_router, prefix=settings.API_STR)

configure_engine(settings)
