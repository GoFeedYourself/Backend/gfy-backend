from sqlalchemy import Table, Column, Integer, String, Text, Float

from database import metadata

food_table = Table(
    'food',
    metadata,
    Column('food_id', Integer, primary_key=True),
    Column('name', String, nullable=False),
    Column('description', Text, nullable=False),
    Column('price', Float, nullable=False),
    Column('weight', String, nullable=False),
)
