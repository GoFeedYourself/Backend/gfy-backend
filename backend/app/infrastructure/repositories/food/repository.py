from typing import List

from sqlalchemy import select
from sqlalchemy.exc import SQLAlchemyError

import database as db
from domain.entities import FoodEntity
from domain.interfaces.repositories import IFoodRepository
from infrastructure.repositories.food.mappers import build_food_entity
from models.food import food_table


class FoodRepository(IFoodRepository):

    def get_list(self, limit: int = 100, skip: int = 0) -> List[FoodEntity]:
        session = db.Session()
        try:
            query = select([food_table]).offset(skip).limit(limit)
            food_list = session.execute(query)
        except SQLAlchemyError:
            session.rollback()
            raise
        else:
            return [build_food_entity(food_entity) for food_entity in food_list]
