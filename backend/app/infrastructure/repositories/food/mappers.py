from sqlalchemy.engine import RowProxy

from domain.entities import FoodEntity


def build_food_entity(food_obj: RowProxy) -> FoodEntity:
    return FoodEntity(
        id=food_obj['id'],
        name=food_obj['name'],
        description=food_obj['description'],
        price=food_obj['price'],
        weight=food_obj['weight']
    )
