import click
import gunicorn


@click.group()
def cli() -> None:
    pass


@click.command()
def serve() -> None:
    pass


if __name__ == '__main__':
    cli()
