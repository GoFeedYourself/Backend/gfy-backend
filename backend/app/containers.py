from dependency_injector import containers, providers

from domain.interfaces.repositories import IFoodRepository
from domain.services.food import FoodService
from infrastructure.repositories.food.repository import FoodRepository


class Repositories(containers.DeclarativeContainer):
    food: IFoodRepository = providers.Singleton(FoodRepository)


class DomainServices(containers.DeclarativeContainer):
    food = providers.Singleton(FoodService, Repositories.food)
